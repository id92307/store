#!/usr/bin/env bash
# VPS 初始化脚本

COLOR_RED="\033[91m" && COLOR_GREEN="\033[92m" && COLOR_NONE="\033[0m" && UNDERLINE="\033[4m" && NO_UNDERLINE="\033[24m"
alias cp='\cp'
alias mv='\mv'
alias rm='\rm'

BASE_DIR=`pwd`

# #----------------################
# 准备阶段
ln -sf /bin/bash /bin/sh
shopt -s expand_aliases
alias wget='wget --no-check-certificate'
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6

# 默认下载点
HOST='https://gitlab.com/id92307/store/raw/master'
# 若在VPS上则删除部分非必要软件
VPSMODE=0

# 读取选项参数
for arg in $@
do
    if [[ "$arg" == "-vps" ]]; then
        VPSMODE=1
    fi
    if [[ "${arg:0:1}" == "@" ]]; then
        HOST=${arg:1}
    fi
done

echo -e "Download files form ${COLOR_GREEN}$HOST${COLOR_NONE}."
echo -e "Yes or not?[Y/N]"
read
if [[ ! "$REPLY" == "y" ]] && [[ ! "$REPLY" == "Y" ]]; then
    echo -e "${COLOR_RED}Canceled.${COLOR_NONE}"
    exit
fi

# 检查系统类型
egrep -i "debian" /etc/issue /proc/version >/dev/null && RELEASE='debian';
egrep -i "ubuntu" /etc/issue /proc/version >/dev/null && RELEASE='ubuntu';
whereis -b yum | grep '/yum' >/dev/null && RELEASE='centos';

if [[ "$RELEASE" == "centos" ]]; then
    if egrep -q "release 6" /etc/redhat-release; then
        service iptables stop
        chkconfig iptables off
        sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
        grep --color=auto '^SELINUX' /etc/selinux/config
        setenforce 0
        
        yum clean all && rm -rf /var/cache/yum
        cd /etc/yum.repos.d/
        mkdir -p bak && mv -f *.repo bak
        curl $HOST/ct6.repo > ct6.repo
        yum -y install epel-release
        rm -rf /etc/yum.repos.d/epel*
    elif egrep -q "release 7" /etc/redhat-release; then
        systemctl disable firewalld
        systemctl stop    firewalld
        systemctl disable NetworkManager
        systemctl stop    NetworkManager
        systemctl enable  network
        systemctl restart network
        firewall-cmd --state
        sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
        grep --color=auto '^SELINUX' /etc/selinux/config
        setenforce 0
        
        yum clean all && rm -rf /var/cache/yum
        cd /etc/yum.repos.d/
        mkdir -p bak && mv -f *.repo bak
        curl $HOST/ct7.repo > ct7.repo
        yum -y install epel-release
        rm -rf /etc/yum.repos.d/epel*
    else
        echo "OS not support"
    fi
    yum -y install wget
elif [[ "$RELEASE" == "ubuntu" ]] || [[ "$RELEASE" == "debian" ]]; then
    apt-get -y install wget
else
    echo -e "${COLOR_RED}OS Not Support.${COLOR_NONE}"
    exit 1
fi

# #----------------################
# rc
cd ~
wget $HOST/my.rc -O .myrc
wget $HOST/myvim.rc -O .vimrc
wget https://raw.githubusercontent.com/trapd00r/LS_COLORS/master/LS_COLORS -O .dircolors
sed -i '/dircolors -b $HOME\/.dircolors/d' .bashrc
echo 'eval $(dircolors -b $HOME/.dircolors)' >> .bashrc
source .bashrc

# 更新系统
if [[ "$RELEASE" == "centos" ]]; then
    if [[ "$VPSMODE" == 1 ]]; then
            yum -y remove apache2*
            yum -y remove bind9*
            yum -y remove font*
            yum -y remove ftp
            yum -y remove httpd*
            yum -y remove locate
            yum -y remove lynx*
            yum -y remove memtester
            yum -y remove nscd-*
            yum -y remove odbcinst*
            yum -y remove portmap
            yum -y remove postfix
            yum -y remove rpcbind
            yum -y remove samba*
            yum -y remove sendmail*
            yum -y remove tcpdump
            yum -y remove tcsh*
            yum -y remove telnet*
            yum -y remove ttf-*
            yum -y remove unixodbc
    fi
    yum -y remove chrony
    yum -y autoremove
    yum clean all
    rm -rf /var/lib/yum/history/*.sqlite
    rm -rf /var/cache/yum
    yum makecache
# ---------------------
    yum -y update
    yum -y upgrade
    yum -y install yum-utils
    yum -y install yum-fastestmirror
    yum -y install curl
    yum -y install bc
    yum -y install screen
    yum -y install python
    #yum -y install git
    yum -y install vim
    yum -y install zip
    yum -y install unzip
    yum -y install lrzsz
    yum -y install mlocate
    yum -y install htop
    yum -y install jnettop
    yum -y install net-tools
    #yum -y install e2fsprogs
    #yum -y install bind-utils
    #yum -y install screenfetch
    yum -y install pciutils # for screenfetch-dev
    wget -O /usr/bin/screenfetch https://raw.githubusercontent.com/KittyKatt/screenFetch/master/screenfetch-dev
    chmod a+x /usr/bin/screenfetch
    yum -y install openssh-server

elif [[ "$RELEASE" == "ubuntu" ]] || [[ "$RELEASE" == "debian" ]]; then
    if [[ "$VPSMODE" == 1 ]]; then
            apt-get -y purge apache2*
            apt-get -y purge bind9*
            apt-get -y purge font*
            apt-get -y purge ftp
            apt-get -y purge httpd*
            apt-get -y purge locate
            apt-get -y purge lynx*
            apt-get -y purge memtester
            apt-get -y purge nscd-*
            apt-get -y purge ntp*
            apt-get -y purge odbcinst*
            apt-get -y purge portmap
            apt-get -y purge postfix
            apt-get -y purge rpcbind
            apt-get -y purge samba*
            apt-get -y purge sendmail*
            apt-get -y purge tcpdump
            apt-get -y purge tcsh*
            apt-get -y purge telnet*
            apt-get -y purge ttf-*
            apt-get -y purge unixodbc
            apt-get -y autoremove
    fi
    apt-get -y clean all
# ---------------------
    apt-get -y update
    apt-get -y upgrade
    apt-get -y install apt-transport-https
    apt-get -y install curl
    apt-get -y install bc
    apt-get -y install screen
    apt-get -y install python
    #apt-get -y install git
    apt-get -y install vim
    apt-get -y install zip
    apt-get -y install lrzsz 
    apt-get -y install mlocate
    apt-get -y install htop
    apt-get -y install jnettop
    apt-get -y install net-tools
    #apt-get -y install e2fsprogs
    #apt-get -y install dnsutils
    #apt-get -y install screenfetch
    wget -O /usr/bin/screenfetch https://raw.githubusercontent.com/KittyKatt/screenFetch/master/screenfetch-dev
    chmod a+x /usr/bin/screenfetch
    apt-get -y install openssh-server
fi

# rc.local
rc_local='/etc/rc.local'

#-- systemd

touch ${rc_local}
chmod a+x ${rc_local}

echo "#!/bin/bash" > tmpfile
echo -e "" >> tmpfile
cat ${rc_local} >> tmpfile
cat tmpfile > ${rc_local}
rm -f tmpfile

cat > /etc/systemd/system/rc-local.service <<-EOF
[Unit]
Description="/etc/rc.local Compatibility"

[Service]
Type=forking
ExecStart=${rc_local} start
TimeoutSec=0
StandardInput=tty
RemainAfterExit=yes
SysVStartPriority=99
 
[Install]
WantedBy=multi-user.target
EOF

systemctl enable rc-local.service

#--

if [[ "$RELEASE" == "centos" ]]; then
    rc_local='/etc/rc.d/rc.local'
    cat ${rc_local} > tmpfile
    rm -f /etc/rc.local /etc/rc.d/rc.local
    mv tmpfile /etc/rc.d/rc.local
    ln -sf /etc/rc.d/rc.local /etc/rc.local
    chmod 755 /etc/rc.d/rc.local /etc/rc.local
fi

sed -i '/exit 0/d' ${rc_local}

# 配置SSHD
mkdir -p /root/.ssh/
cd /root/.ssh
wget $HOST/key.pub -O authorized_keys.tmp && mv -f authorized_keys.tmp authorized_keys

cd /etc/ssh/
cp sshd_config sshd_config.bak.`date +%y%m%d%H%M%S`
wget $HOST/sshd.conf -O sshd_config.tmp && mv -f sshd_config.tmp sshd_config
echo "Subsystem   sftp    `find  /usr -name sftp-server -type f`" >> sshd_config

rm -rf ssh_host_*_key*
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -t dsa -N '' -C '' -q
ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -t ecdsa -N '' -C '' -q
ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -t ed25519 -N '' -C '' -q
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -t rsa -N '' -C '' -q

chmod 400 /etc/ssh/*

# 配置时区
mkdir -p /etc/sysconfig/
echo "ZONE=\"Asia/Shanghai\"" > /etc/sysconfig/clock
echo "UTC=true" >> /etc/sysconfig/clock
echo "ARC=false" >> /etc/sysconfig/clock
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# #----------------################
# 收尾
cd $BASE_DIR
if [[ "$RELEASE" == "centos" ]]; then
    yum -y autoremove
    yum-complete-transaction -y
    yum -y clean all
else
    apt-get -y autoremove
    apt-get -y autoclean
    apt-get -y clean all
fi
updatedb

echo -e "${COLOR_GREEN}ALL DONE.${COLOR_NONE}"

